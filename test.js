"use strict";

var params = browser.params;

describe('test of page', function() {
	it('get to page', function() {
		browser.get('web:8080');
	});

	it('check text', function() {
		var e = element(by.id('test'));
		expect(e.isDisplayed()).toBeTruthy();

	expect(e.getText()).toContain('world');
	});
});
