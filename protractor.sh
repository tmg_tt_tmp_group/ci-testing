#!/bin/bash

#trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

xvfb-run -a --server-args="-screen 0 ${SCREEN_RES}" npm run server > ./server.log 2>&1 &
sleep 10

npm -s run test
