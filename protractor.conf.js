"use strict";

exports.config = {
	chromeOnly: true,
	framework: 'jasmine',
	seleniumAddress: 'http://localhost:4444/wd/hub',
	suites: {
		baset: 'test.js',
	},
	params: {
		window: {
			position: { x:0, y:0 },
			size: "max"
		},
		run: {
			failFast: false,
			htmlLog: true
		}
	},

	jasmineNodeOpts: {
		print: function() {},
		showColors: true,
		defaultTimeoutInterval: 360000,
		isVerbose: false,
		includeStackTrace: false
	},

	onPrepare: function() {
		var winPos = browser.params.window.position;
		browser.driver.manage().window().setPosition(winPos.x, winPos.y);
		if (browser.params.window.size === "max") {
			browser.driver.manage().window().maximize();
		}
		browser.ignoreSynchronization = true;
		browser.driver.manage().timeouts().implicitlyWait(1000); // FIXME!!! try with value 0

		if (browser.params.run.htmlLog) {
			var JasmineHtml = require('protractor-jasmine2-html-reporter');
			jasmine.getEnv().addReporter(
				new JasmineHtml({
					savePath: "./reports/",
					screenshotsFolder: 'screens/',
					takeScreenshotsOnlyOnFailures: true
				})
			);
		}

		// need to strip of useless description of error
		var myConsolePrint = function(s) {
			if (s && s.length !== 0 && s.replace(/\s/g,"") !== "") {
				var lines = s.split(/\r?\n/);
				if (!lines || lines.length<4 ) {
					process.stdout.write(s + "\n");
				} else {
					var supressLines = /(^\(Session info\:)|(^\(Driver info\:)|(^Command duration or timeout\:)|(^Build info\:)|(^System info\:)|(^Driver info\:)|(^Capabilities)|(^Session ID\:)/;
					for (var i = 0; i < lines.length; i++) {
						var l = lines[i].trim();
						if (l.match(supressLines) === null) {
							process.stdout.write(lines[i] + "\n");
						}
					}
				}
			}
		};

		// that is pretty console reported
		var JasmineConsole = require('jasmine-console-reporter');
		jasmine.getEnv().addReporter(
			new JasmineConsole({
			colors: 1,
			cleanStack: 3,
			verbosity: 3,
			listStyle: 'indent',
			activity: false,
			print: myConsolePrint
		}));

		// to break execution on any failure
		if (browser.params.run.failFast) {
			var failFast = require('jasmine-fail-fast');
			jasmine.getEnv().addReporter(failFast.init());
		}
	}
};
